## WWORKFLOW PROJECT: Kraken2 classification workflow
## INIT DATE: 2018
import pandas as pd
import glob, os, os.path, datetime, sys, csv
from os.path import join, abspath
from snakemake.utils import validate, min_version

##### set minimum snakemake version #####
min_version("5.9.1")

## =============================================================================
## SETUP
## =============================================================================

## SET SOME DEFAULT PATH
SCHEMAS = abspath("schemas")
SCRIPTS = abspath("scripts")
WRAPPER = abspath("wrapper")
ENVS    = abspath("envs")

## LOAD VARIABLES FROM CONFIGFILE
## config-file needs to be submitted on command-line via --configfile
if config=={}:
    print('Please submit config-file with "--configfile <file>". Exit.')
    sys.exit(1)

# specify config file with --configfile
# validate config file
validate(config, schema=join(SCHEMAS, "config.schema.yaml"))
## print the config to stderr
sys.stderr.write("********** Submitted config: **********\n")
for k,v in config.items():
    sys.stderr.write("{}: {}\n".format(k,v))
sys.stderr.write("***************************************\n")

## define global Singularity image for reproducibility
## USE: "--use-singularity --use-conda" to run all jobs in container
singularity: config["singularity"]

BASEDIR      = abspath(config["resultdir"])
RES          = join(BASEDIR, "results")
LOGDIR       = join(BASEDIR, "logs")
BENCHMARKDIR = join(BASEDIR, "benchmarks")

# TOOLS
# Kraken2
KRAKEN_DB    = config["kraken2"]["db"]
KRAKEN_EXTRA = config["kraken2"]["extra"]

KRONA_TAXDB  = config["krona"]["tax"]
KRONA_EXTRA  = config["krona"]["extra"]

## project specific outs
KRAKENDIR    = join(RES, "01_kraken2")
BRACKENDIR   = join(RES, "02_bracken")
BRACKENDIR2  = join(RES, "03_bracken_matrix")
BRACKENDIR3  = join(RES, "04_bracken_matrix_pct")
KRONADIR     = join(RES, "05_krona_per_level_per_sample")
KRONADIR2    = join(RES, "06_krona_per_level_per_condition")

BRACKEN_RLEN  = config["bracken"]["readlen"]
BRACKEN_LEVEL = [level.strip() for level in config["bracken"]["level"].split(',')]
for level in BRACKEN_LEVEL:
    if level not in ["D","P","C","O","F","G","S"]:
        sys.stderr.write("Level '{}' from configfile is invalid. Needs to be one of D,P,C,O,F,G,S. Exit\n".format(level))
        sys.exit()

# SAMPLES
## sample inputs
SAMPLESHEET  = abspath(config["samples"])

# validate samplesheet
samples = pd.read_table(SAMPLESHEET).set_index("sample", drop=False)
validate(samples, schema=join(SCHEMAS, "samples.schema.yaml"))

## =============================================================================
## LOAD SAMPLES, check if files exist
sys.stderr.write('Reading samples from samplesheet: "{}" ...\n'.format(SAMPLESHEET))

# test if sample in dir
for fname in samples["r1"]:
    if not os.path.isfile(fname):
        sys.stderr.write("File '{}' from samplesheet can not be found. Make sure the file exists. Exit\n".format(fname))
        sys.exit()

for fname in samples["r2"]:
    if type(fname) == str:
        if not os.path.isfile(fname):
            sys.stderr.write("File '{}' from samplesheet can not be found. Make sure the file exists. Exit\n".format(fname))
            sys.exit()


NUM_SAMPLES = len(samples["sample"])
sys.stderr.write('{} samples to process\n'.format(NUM_SAMPLES))

SAMPLES = list(samples["sample"])
CONDITIONS = set(samples["condition"])

## =============================================================================
## FUNCTIONS
## =============================================================================
def get_fasta(wildcards):
    # does not return fq2 if it is not present
    return samples.loc[(wildcards.sample), ["r1", "r2"]].dropna()


## get samples belonging to a condition
def get_brackensamples_per_condition(wildcards):
    temp_s = list(samples[samples["condition"] == wildcards.condition]["sample"].unique())
    temp_s.sort()
    return [join(BRACKENDIR, "%s/%s.bracken.txt"%(wildcards.level, s)) for s in temp_s]

## =============================================================================
## RULES
## =============================================================================

## Pseudo-rule to stae the final targets, so that the whole workflow is run.
rule all:
    input:
        expand(join(BRACKENDIR3, "all.bracken.{level}.pct.txt.gz"), level=BRACKEN_LEVEL),
        expand(join(KRONADIR2,"krona.{level}.{condition}.html"), level=BRACKEN_LEVEL, condition=CONDITIONS),
        expand(join(KRONADIR,"krona.{level}.html"), level=BRACKEN_LEVEL)

        
rule kraken2:
    input:
        sample=get_fasta
    output:
        out=join(KRAKENDIR, "{sample}/kraken2.txt.gz"),
        report=join(KRAKENDIR, "{sample}/kraken2.report.txt")
    log:
        join(LOGDIR, "kraken2/{sample}.log")
    benchmark:
        join(BENCHMARKDIR, "kraken2/{sample}.txt")
    threads: 16
    conda:
        join(ENVS, "kraken2.yaml")
    params:
        extra=KRAKEN_EXTRA,
        db=KRAKEN_DB
    wrapper:
        "file://wrapper/kraken2/wrapper.py"
          

rule bracken:
    input:
        report=join(KRAKENDIR, "{sample}/kraken2.report.txt")
    output:
        join(BRACKENDIR, "{level}/{sample}.bracken.txt")
    log:
        err=join(LOGDIR, "bracken_{level}/{sample}.err"),
        out=join(LOGDIR, "bracken_{level}/{sample}.out")
    benchmark:
        join(BENCHMARKDIR, "bracken_{level}/{sample}.txt")
    conda:
        join(ENVS, "kraken2.yaml")
    params:
        level="{level}",
        rlen=BRACKEN_RLEN 
    shell:
        "bracken -d {KRAKEN_DB} -i {input} -o {output} -r {params.rlen} -l {params.level} 2> {log.err} > {log.out}"


rule bracken_matrix:
    input:
        [join(BRACKENDIR, "{level}/%s.bracken.txt"%(s)) for s in SAMPLES]
    output:
        join(BRACKENDIR2, "all.bracken.{level}.txt.gz")
    log:
        join(LOGDIR, "bracken_counts_{level}.log")
    benchmark:
        join(BENCHMARKDIR, "bracken_counts_{level}.txt")
    params:
        script=join(SCRIPTS, "build_matrix.py")
    shell:
        "python {params.script} {input}  2> {log} | gzip > {output}" 
        

rule bracken_matrix_pct:
    input:
        join(BRACKENDIR2, "all.bracken.{level}.txt.gz")
    output:
        join(BRACKENDIR3, "all.bracken.{level}.pct.txt.gz")
    log:
        join(LOGDIR, "matrix_pct/{level}.log")
    benchmark:
        join(BENCHMARKDIR, "matrix_pct/{level}.txt")
    conda:
        join(ENVS, "pandas.yaml")
    params:
        script=join(SCRIPTS, "matrix_pct.py"),
        extra=r'--header --index "1,2"'
    shell:
        "python {params.script} {params.extra} {input} 2> {log} | gzip > {output}"     


rule krona_per_level_per_sample:
    input:
        files=[join(BRACKENDIR, "{level}/%s.bracken.txt"%(s)) for s in SAMPLES]
    output:
        join(KRONADIR,"krona.{level}.html")
    log:
        out=join(LOGDIR, "krona_per_level_per_sample/{level}.out"),
        err=join(LOGDIR, "krona_per_level_per_sample/{level}.err")
    benchmark:
        join(BENCHMARKDIR, "krona_per_level_per_sample/{level}.txt")
    conda:
        join(ENVS, "krona.yaml")
    params:
        extra=KRONA_EXTRA,
        db=KRONA_TAXDB
    shell:
        "ktImportTaxonomy {params.extra} -tax {params.db} -s 6 -t 2 {input.files} -o {output} > {log.out} 2> {log.err}"
        
        
rule krona_per_level_per_condition:
    input:
        files=get_brackensamples_per_condition
    output:
        join(KRONADIR2,"krona.{level}.{condition}.html")
    log:
        out=join(LOGDIR, "krona_per_level_per_condition/{level}.{condition}.out"),
        err=join(LOGDIR, "krona_per_level_per_condition/{level}.{condition}.err")
    benchmark:
        join(BENCHMARKDIR, "krona_per_level_per_condition/{level}.{condition}.txt")
    conda:
        join(ENVS, "krona.yaml")
    params:
        extra=KRONA_EXTRA,
        db=KRONA_TAXDB
    shell:
        "ktImportTaxonomy {params.extra} -tax {params.db} -c -s 6 -t 2 {input.files} -o {output} > {log.out} 2> {log.err}"

## rule matrix_vst:
##     input:
##         counts = join(BRACKENDIR2, "all.bracken.{level}.txt.gz")
##     output:
##         join(VSTDIR, "all.bracken.{level}.vst.txt.gz"),
##         join(VSTDIR, "all.bracken.{level}.vst.dispersion.rds")
##     log:
##         join(LOGDIR, "matrix_vst/{level}.log")
##     benchmark:
##         join(BENCHMARKDIR, "matrix_vst/{level}.txt")
##     threads: 16
##     conda:
##         join(ENVS, "deseq2.yaml")
##     params:
##         extra=r''
##     script:
##         join(SCRIPTS, "deseq2.R")



         
rule clean:
    shell:
        "rm -rf {BASEDIR}/*"

