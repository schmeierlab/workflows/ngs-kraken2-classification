# Snakemake workflow: Kraken2 classification

[![https://www.singularity-hub.org/static/img/hosted-singularity--hub-%23e32929.svg](https://www.singularity-hub.org/static/img/hosted-singularity--hub-%23e32929.svg)](https://singularity-hub.org/collections/4110) 

- AUTHOR: Sebastian Schmeier (s.schmeier@pm.me)
- DATE: 2020 
- VERSION: 1.3.1

## OVERVIEW

- Classify reads from either fastq- or fasta-files with Kraken2
- Estimate abundances of reads at certain phyla-level with Bracken
- Summarise the abundances in a matrix form for all samples.
- Calculate a matrix of pct for each sample.
- Produce a krona plot for each sample.
- If different conditions are present in the samplesheet, produce a Krona plot for each condition.

## INSTALL WORKFLOW

```bash
# Install miniconda
# LINUX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
# MACOSX:
curl -O https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh

# reopen shell

# Make env
# this environment contains the bare base of what is required to run snakemake
conda env create --name snakemake --file envs/snakemake.yaml
conda activate snakemake

# Clone repo
git clone https://gitlab.com/schmeierlab/workflows/ngs-kraken2-classification.git
```

## INSTALL TEST DATA (optional)

Download test data set with:

```bash
git clone https://gitlab.com/schmeierlab/workflows/ngs-test-data.git
```

## DOWNLOAD MINIKRAKEN2 AND BRACKEN DATABASES

Adjust the config-file accordingly.

```bash
# we use the non-human version here
mkdir minikraken2
cd minikraken2
wget ftp://ftp.ccb.jhu.edu/pub/data/kraken2_dbs/minikraken2_v2_8GB_201904_UPDATE.tgz -O minikraken2.tgz
tar xvzf minikraken2.tgz
rm minikraken2.tgz
wget https://ccb.jhu.edu/software/bracken/dl/minikraken2_v2/database100mers.kmer_distrib
wget https://ccb.jhu.edu/software/bracken/dl/minikraken2_v2/database150mers.kmer_distrib
wget https://ccb.jhu.edu/software/bracken/dl/minikraken2_v2/database200mers.kmer_distrib
cd -
```

## BUILD YOUR OWN KRAKEN2/BRACKEN DATABASES (ALTERNATIVE)

```bash
kraken --db=bacteria --threads=6 bacteria/library/crc_bacteria/*.fna  > bacteria/database.kraken

conda create --yes -n bracken bracken
conda activate bracken

kmer2read_distr --seqid2taxid bacteria/seqid2taxid.map --taxonomy bacteria/taxonomy --kraken bacteria/database.kraken --output bacteria/database100mers.kraken -t6 -k31 -l100

generate_kmer_distribution.py -i bacteria/database100mers.kraken -o bacteria/database100mers.kmer_distrib
```

## UPDATE KRONA TAXONOMY

Krona is a bit difficult sometimes.
We provide a Krona taxonomy file in `data/taxonomy`.
However, if you wish to update this (which might be a good idea), use this:

```bash
conda env create -n krona -f envs/krona.yaml
conda activate krona
bzip2 -d data/taxonomy/taxonomy.tab.bz2
ktUpdateTaxonomy.sh data/taxonomy
conda deactivate
```

## RUN SNAKEMAKE WORKFLOW

Change the `config.yaml` to suit your data.

```bash
# Do a dryrun of the workflow, show rules, order, and commands
snakemake -np --use-conda --cores 16 --configfile config.yaml

# If necessary bind more folders for singularity outside of your home
# conda-only run
snakemake -p --use-conda --cores 16 --configfile config.yaml 2> run.log

# singularity run
snakemake -p --use-singularity --singularity-args "--bind /mnt/DATA" --configfile config.yaml 2> run.log

# show a detailed summary of the produced files and used commands
snakemake -D

# To delete all created files use
snakemake -p clean
```

## Information

### Bracken

The pipeline is running kraken2 for every sample.
Subsequently, a sample's `kraken2.report.txt` is input to bracken to estimate count abundances for each desired level (species, genus, etc.).
One such run produces a modified `kraken2.report_bracken.txt` in addition to the desired output file.
It will be overwritten by subsequent bracken run for a different level, thus do not rely on the modified report as its not ensured what level this was produced at.
It is not used further in this pipeline and it is not recommended to be used as it cannot be ensured from which run it was created.
However, if you only run one level, the file is good to be used.

## TODO

- optional sample trimming with fastp
